#HowTo: Tensorflow/Jupyter/Bitbucket

#pull the image(~6GB)
docker pull jupyter/tensorflow-notebook

#Run the container (put your own path for -v option)
docker run -d --rm --name tf -p8888:8888  -v "/Users/hamid/Google Drive/Tasks":/notebooks dartanalyzer/ibproto:latest-py3

#Version control using Bitbucket (we choose this over github since it allows unlimited private repos). Very similar process applies to github.

#create a repo on Bitbucket
https://confluence.atlassian.com/bitbucket/create-a-git-repository-759857290.html

#open Terminal and install git on your host OSX using Homebrew (http://brew.sh/)
brew install git

#clone the repo locally. get the clone link from the repo webpage. Make you use HTTPS link (not SSH). Then change to the cloned repo directory for later commands.
#private repo requires Bitbucket account password
git clone https://dartanalyzer@bitbucket.org/dartanalyzer/tasty.git
cd tasty

#set your name and email so that git can track who changed what
git config --global user.name "Your Name"
git config --global user.email "you@example.com"

#browse to http://localhost:8888/. change directory to the cloned repo. make notebooks and start coding. once finished add all the files you want to be tracked by Git to the repository.
git add Untitled.ipynb

#change the file content and save (commit) it. after -m the commit message comes. 
git commit -a -m 'Initial commit'

#push changes to Bitbucket. Requires Bitbucket account password.(It is possible to bypass password prompt at the cost of some configurations)
git push origin master 

#update your code. commit and push.
#Bitbucket does not have .ipynb file viewer. To view your notebook online go to Bitbucket repo. open your file in the "raw" format. copy the link. paste it on nbviewer.jupyter.org

#if someone else made changes and you want to get the latest version from the cloud
git pull





