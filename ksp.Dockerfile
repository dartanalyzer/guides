FROM continuumio/anaconda3

RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

RUN pip install treeinterpreter
RUN pip install PyMySQL

#install jupyter
RUN /opt/conda/bin/conda install jupyter -y --quiet 

CMD [ "/bin/bash" ]