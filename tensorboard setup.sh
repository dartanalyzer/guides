#start the container with tensorboard port mapping
docker run -d --rm --name nb -p8889:8888  -p6006:6006 -v "/Users/hamid/Google Drive/Tasks":/notebooks dartanalyzer/ibproto:latest-py3

#go to container shell
docker exec -it nb bash

#make a log directory and run tensor board
root@8a8a03069cc7:/notebooks# mkdir /llog
root@8a8a03069cc7:/notebooks# tensorboard --logdir=/llog 
Starting TensorBoard b'55' at http://8a8a03069cc7:6006

#Now it should be accessible at http://localhost:6006