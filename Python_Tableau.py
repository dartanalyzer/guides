#How to call python code from Tableau

#run ibproto (python 2) container
docker run -d --rm --name nb2 -p8888:8888 -v "/Users/hamid/Google Drive/Tasks":/notebooks dartanalyzer/ibproto:latest

#install Tabpy server from https://github.com/tableau/TabPy
#TBD

#Open a notebook on the container. The following are the code in Jupyter cells
#install client software
!pip install tabpy-client
!pip install future
#connect to the server
import tabpy_client
client = tabpy_client.Client('http://localhost:9004/')
#write a sample function
def clustering(x, y):
    import numpy as np
    from sklearn.cluster import DBSCAN
    from sklearn.preprocessing import StandardScaler
    X = np.column_stack([x, y])
    X = StandardScaler().fit_transform(X)
    db = DBSCAN(eps=1, min_samples=3).fit(X)
    return db.labels_.tolist()
 #deploy it on the server
client.deploy('clustering',
                clustering,
                'Returns cluster Ids for each data point specified by the pairs in x and y')
#call it from notebook to test the server
x = [6.35, 6.40, 6.65, 8.60, 8.90, 9.00, 9.10]
y = [1.95, 1.95, 2.05, 3.05, 3.05, 3.10, 3.15]
client.query('clustering', x, y)
#an acceptable responce would be:
# {u'model': u'clustering',
#  u'response': [0, 0, 0, 1, 1, 1, 1],
#  u'uuid': u'316b37a9-fde8-4c42-9085-9afca0689478',
#  u'version': 1}

#On Tableau desktop. goto Help > Setting and Performance > Manage External serive Connection ...
#input 'localhost' as the Server and '9004' as port. test the connection and press OK

#use Excel to make a sheet with two columns (headers: X and Y ) e.g. with the values used above
#load the file as a datasource in Tableau
# Make a calculated field using the following code
SCRIPT_INT("return tabpy.query('clustering',_arg1,_arg2)['response']",ATTR([X]),ATTR([Y]))

#The above line sends ATTR([X]),ATTR([Y] as _arg1 and _arg2 respectively to the clustering function on Tabpy server and get the response list.
#As the results are integer we used SCRIPT_INT function


