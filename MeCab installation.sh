#MeCab installation

#start the container(OS: Ubuntu 16.04.2 LTS)
docker run -d --rm -p [PORT_NUM]:8888 -v [PATH]:/notebooks tensorflow/tensorflow

#Open a shell on the container
docker exec -it [CONT_ID] bash

#install 'wget'
apt-get update
apt-get install wget

#download, decompress, compile and install MeCab
wget -O mecab-0.996.tar.gz "https://drive.google.com/uc?export=download&id=0B4y35FiV1wh7cENtOXlicTFaRUE"
tar zxvf mecab-0.996.tar.gz
cd mecab-0.996 && ./configure && make && make install 

#load MeCab shared libs just installed
ldconfig

##download, decompress, compile and install MeCab ipadic
wget -O mecab-ipadic-2.7.0-20070801.tar.gz "https://drive.google.com/uc?export=download&id=0B4y35FiV1wh7MWVlSDBCSXZMTXM"
tar zxvf mecab-ipadic-2.7.0-20070801.tar.gz
cd mecab-ipadic-2.7.0-20070801 &&./configure --with-charset=utf8 && make && make install

#test it: 
#type the command to get the input prompt
mecab
#paste this string in and press enter
これはテストです